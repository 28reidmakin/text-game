# Game Engine

[![npm](https://img.shields.io/npm/v/@yepreally/text-game)](https://www.npmjs.com/package/@yepreally/text-game)
[![npm](https://img.shields.io/npm/dw/@yepreally/text-game)](https://www.npmjs.com/package/@yepreally/text-game)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/yepreally/text-game?branch=main)](https://gitlab.com/yepreally/text-game/-/pipelines)
[![Gitlab code coverage](https://img.shields.io/gitlab/coverage/yepreally/text-game/main)](https://gitlab.com/yepreally/text-game/-/graphs/main/charts)
[![Snyk Vulnerabilities for npm package](https://img.shields.io/snyk/vulnerabilities/npm/@yepreally/text-game)](https://snyk.io/advisor/npm-package/@yepreally/text-game)

## Getting Started

### Usage
```npx @yepreally/text-game ./my/story.txt```

### Story Format
```// TODO Document```