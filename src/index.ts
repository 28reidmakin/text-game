#!/usr/bin/env node
import { readFileSync, existsSync } from 'fs';
import { resolve } from 'path';
import { Choice, prompts } from 'prompts';
import { IScene, IOption } from './types';
import { parseStory } from './parsers/storyParser';

if (!process.argv[2]) {
  throw new Error(`Story path must be provided. For Example:\nnpx @jedfong/game-engine ./my/story.txt`);
}

const storyFilePath = resolve(process.cwd(), process.argv[2]);
if (!existsSync(storyFilePath)) {
  throw new Error(`Story path must exist, but ${storyFilePath} was not found`);
}

const gameText = readFileSync(storyFilePath, 'utf-8');

const story = parseStory(gameText);

const initialScene = Object.values(story).find(scene => scene.state === 'start');

if (!initialScene) {
  throw new Error(`One of the scenes must specify the "start" state. For example:
  scene: some_scene_id => start
  or
  scene: some_scene_id => state=start`);
}

const displayPrompt = async(scene: IScene): Promise<IScene | null> => {
  const message = scene.script.reduce<string>((acc, script) => {
    if (!acc) {
      return script;
    }

    return acc += `\n${script}`;
  }, '');

  let choices: Choice[] = [{
    title: 'The End',
    value: 'complete',
    selected: true,
  }];

  const isEnd = scene.options.length === 0;
  if (!isEnd) {
    choices = scene.options.map<Choice>((option: IOption) => ({
      title: option.label,
      value: option.goto,
      selected: scene.options.length === 1,
    }));
  }

  const selectedValue = await prompts.select({
    type: 'select',
    name: 'option',
    message,
    choices,
    instructions: false,
    hint: '',
  }) as unknown as string;

  const nextScene = story[selectedValue];
  return (!isEnd && nextScene) ? nextScene : null;
};

(async() => {
  let scene: IScene | null = initialScene;
  let isComplete = false;
  while (!isComplete) {
    // eslint-disable-next-line no-await-in-loop
    scene = await displayPrompt(scene as IScene);
    if (!scene) {
      isComplete = true;
    }
  }
})();

