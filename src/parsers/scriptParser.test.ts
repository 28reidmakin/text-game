import { parseScript } from './scriptParser';

describe('parseScript', () => {
  test('when `script:` prefix', () => {
    expect(parseScript('script:')).toEqual('');
  });
  test('when `script:` prefix', () => {
    expect(parseScript('script:intro ')).toEqual('intro ');
  });
  test('when `script:` prefix with space after colon', () => {
    expect(parseScript('script: intro ')).toEqual('intro ');
  });
  test('when no `script:` prefix', () => {
    expect(parseScript(' intro ')).toEqual(' intro ');
  });
});
