interface IScriptRegexGroups {
  script: string;
}

export const parseScript = (line: string): string => {
  const { groups } = line.match(/^(script:\s?)?(?<script>.*)$/) as unknown as { groups: IScriptRegexGroups };

  return groups.script;
};
