import { IEffect, IOption } from '../types';

interface IOptionRegexGroups {
  label: string;
  effects?: string;
}

interface IEffectRegexGroup {
  operation: '+' | '-' | '*' | '/';
  value: string;
}

const GOTO_KEY = 'goto';

export const parseGoTo = (key: string, value: string, idx: number): string | null => {
  if (key && !value && idx === 0) {
    return key;
  }

  if (key.toLowerCase() === GOTO_KEY) {
    return value;
  }

  return null;
};

export const parseEffect = (key: string, value: string): IEffect | null => {
  if (key && value) {
    const match = value.match(/^(?<operation>[+-\/\*]?)\s*(?<value>\d*)$/) as unknown as { groups: IEffectRegexGroup };
    const valueInt = parseInt(match?.groups.value, 10);
    const operation = match?.groups.operation || '=';

    if (!isNaN(valueInt)) {
      return { key, operation, value: valueInt };
    }
  }

  return null;
};

export const parseEffects = (rawEffects?: string): { effects: IEffect[], goto: string | null } => {
  return (rawEffects ?? '').split(',').reduce<{ effects: IEffect[], goto: string | null }>((acc, rawEffect, idx) => {
    const [untrimmedKey, untrimmedValue] = rawEffect.split('=');
    const key = untrimmedKey.trim();
    const value = (untrimmedValue ?? '').trim();

    const goto = parseGoTo(key, value, idx);
    if (goto) {
      acc.goto = goto;
      return acc;
    }

    const effect = parseEffect(key, value);
    if (effect) {
      acc.effects.push(effect);
      return acc;
    }

    return acc;
  }, { effects: [], goto: null });
};

export const parseOption = (line: string): IOption => {
  const { groups } = line.match(/^option:\s*(?<label>((?!=>).)*)\s*(=>\s*(?<effects>.*))?$/) as unknown as { groups: IOptionRegexGroups };
  const { effects, goto } = parseEffects(groups.effects);

  return {
    label: groups.label.trim(),
    goto: goto ?? null,
    effects,
  };
};
